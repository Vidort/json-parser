#include "JsonParser.h"
#include <sstream>
using namespace std;
using namespace JSON;

//node
//constructors
Node::Node(vector<Node> _array) : 
    node(move(_array))
{}

Node::Node(map<string, Node> map) :
    node(move(map)) 
{}

Node::Node(int value) : 
    node(value)
{}

Node::Node(string value) : 
    node(move(value))
{}
//constructors


//getters
const vector<Node>& Node::AsArray() const {
    return get<vector<Node>>(node);
}

const map<string, Node>& Node::AsMap() const {
    return get<map<string, Node>>(node);
}

int Node::AsInt() const {
    return get<int>(node);
}

const string& Node::AsString() const {
    return get<string>(node);
}
//getters

NodeType Node::Type()const {
    return static_cast<NodeType>(node.index());
}
//node


//document
Document::Document(Node root) : 
    root(move(root)) 
{}

const Node& Document::GetRoot() const {
    return root;
}

//load
Node LoadNode(istream& input);

Node LoadArray(istream& input) {
    vector<Node> result;

    for (char c; input >> c && c != ']'; ) {
        if (c != ',') {
            input.putback(c);
        }
        result.push_back(LoadNode(input));
    }

    return Node(move(result));
}

Node LoadInt(istream& input) {
    int result = 0;
    while (isdigit(input.peek())) {
        result *= 10;
        result += input.get() - '0';
    }
    return Node(result);
}

Node LoadString(istream& input) {
    string line;
    getline(input, line, '"');
    return Node(move(line));
}

Node LoadMap(istream& input) {
    map<string, Node> result;

    for (char c; input >> c && c != '}'; ) {
        if (c == ',') {
            input >> c;
        }

        string key = LoadString(input).AsString();
        input >> c;
        result.insert({ move(key), LoadNode(input) });
    }

    return Node(move(result));
}

Node LoadNode(istream& input) {
    char c;
    input >> c;

    if (c == '[') {
        return LoadArray(input);
    }
    else if (c == '{') {
        return LoadMap(input);
    }
    else if (c == '"') {
        return LoadString(input);
    }
    else {
        input.putback(c);
        return LoadInt(input);
    }
}


JSON::Document JSON::LoadJson(const std::string& input) {
    istringstream stream(input, ios_base::in);
    return Document(LoadNode(stream));
}

JSON::Document JSON::LoadJson(std::istream& input) {
    return Document(LoadNode(input));
}
//load

//print
void PrintNode(ostream& out, const Node& node);
void PrintArray(ostream& out, const vector<Node>& arr);
void PrintMap(ostream& out,  const map<string, Node>& map);
void PrintString(ostream& out, const string& str) {
    out << '"' << str << '"';
}

void PrintNode(ostream& out, const Node& node) {
    if (node.Type() == NodeType::Array)PrintArray(out, node.AsArray());
    else if (node.Type() == NodeType::Map)PrintMap(out, node.AsMap());
    else if (node.Type() == NodeType::String)PrintString(out, node.AsString());
    else if (node.Type() == NodeType::Int) {
        out << node.AsInt();
    }
}

void PrintMap(ostream& out, const map<string, Node>& map) {
    out << "{";
    const string& last = (--(map.end()))->first;
    for (const auto& [key, node] : map) {
        PrintString(out, key);
        out << ": ";
        PrintNode(out, node);
        if (key != last)out << ", ";
    }
    out << "}";
}

void PrintArray(ostream& out, const vector<Node>& arr) {
    out << "[";
    for (size_t i = 0; i < arr.size(); i++) {
        PrintNode(out, arr[i]);
        if (i < arr.size() - 1)out << ", ";
    }
    out << "]";
}

std::ostream& operator<< (std::ostream& out, const Document& doc) {
    PrintNode(out, doc.GetRoot());
    return out;
}
//print
//document
