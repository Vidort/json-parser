#pragma once

#include <istream>
#include <vector>
#include <string>
#include <map>
#include <variant>

namespace JSON {

enum class NodeType {Array,	Map, Int, String};

class Node  {
	std::variant<
		std::vector<Node>,
		std::map<std::string, Node>,
		int,
		std::string
	> node;
public:
	constexpr Node() = default;
	explicit Node(std::vector<Node> array);
	explicit Node(std::map<std::string, Node> map);
	explicit Node(int value);
	explicit Node(std::string value);

	inline NodeType Type()const;

	inline const std::vector<Node>& AsArray() const;
	inline const std::map<std::string, Node>& AsMap() const;
	inline int AsInt() const;
	inline const std::string& AsString() const;
};

class Document {
public:
	constexpr Document() = default;
	
    inline const Node& GetRoot() const;

	friend Document LoadJson(std::istream& input);
	friend Document LoadJson(const std::string& input);

private:
	explicit Document(Node root);
	Node root;
};

}//JSON

std::ostream& operator<< (std::ostream& out, const JSON::Document& doc);

